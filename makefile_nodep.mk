GITHASH = $(shell git rev-parse --short HEAD)
OPTIONS += -DGITHASH="\"${GITHASH}\""
INCLUDE += $(LOCAL_INCLUDE)

#this file define the var INCLUDE


all: OPTIMISATION = -O2
all: executable

debug: NAME_APPEND =
debug: OPTIMISATION += -g -DDEBUG
debug: executable

profile: NAME_APPEND = _prof
profile: OPTIMISATION += -O2
profile: OPTIONS += -pg
profile: LINK += -pg
profile: executable

executable: $(OBJS)
	$(LINKER) $(LDFLAGS) -o $(EXEC)$(NAME_APPEND) $(OBJS) $(LINK)

-include $(OBJS:.o)

.cpp.o:
	$(COMPILER) $(OPTIMISATION) $(OPTIONS) $(INCLUDE) -c $< > $@
clean:
	rm -f *.o 
