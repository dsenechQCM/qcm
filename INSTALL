This files explains how to compile and install the packages qcm and qcm_ED on a few platforms.

Important: because qcm imports headers defined in qcm_ED, it is important to install qcm_ED
BEFORE installing qcm. qcm will find the path to headers via a call to qcm_ED.

Also, the PYTHONPATH variable must be changed to include the path to the pyqcm module.

-----------------------------------------------------------------------------------------------  
COMMON FEATURES

1) The distributions can be obtained from bitbucket:

git clone https://dsenech@bitbucket.org/dsenech/qcm_ed.git
git clone https://dsenech@bitbucket.org/dsenech/qcm.git
git clone https://dsenech@bitbucket.org/dsenech/pyqcm.git

2) A local lib/ folder should be created, in $HOME.
Then this folder should be added to the PYTHONPATH environment variable, as specified in .bash_profile:

export PYTHONPATH=$PYTHONPATH:$HOME/lib

The pyqcm folder cloned from bitucket should be moved to $HOME/lib/

3) CUBA
fetch CUBA at http://www.feynarts.de/cuba/ (take the latest version) and decompress locally.
then run ./configure and then edit the makefile it produces by adding the option -fPIC 
in two places, i.e. at the end of the lines that start with CFLAGS and LIBS, respectively, like so:

CFLAGS = -O3 -fomit-frame-pointer -ffast-math -Wall -DHAVE_CONFIG_H -DREALSIZE=8 -I$(common) -I. -I$(srcdir) -fPIC
LIBS = -lm -fPIC

This change is necessary if CUBA is to be loaded as part of a dynamic library.
Then run 'make lib' and move the resulting static library 'libcuba.a' to $HOME/lib/
   
4) Go to the folder containing qcm_ED (e.g. libqcm_ed or whatever name you changed it to) and to the folder
appropriate for your platform (mac, CC, linux). Compile with the makefile (or with make -f nodep.mk if you
do not care about tracking dependencies and you want compilation to be faster).
It is important to compile qcm_ED before qcm.

5) Go to the folder containing qcm (e.g. libqcm or whatever name you changed it to) and to the folder
appropriate for your platform (mac, CC, linux). Compile with the makefile (or with make -f nodep.mk if you
do not care about tracking dependencies and you want compilation to be faster).

You should be ready to use pyqcm then.

Below are system-specific instructions.

-----------------------------------------------------------------------------------------------  
LINUX (UBUNTU LTS 16.04)

Dependencies can be installed with the following:

sudo apt-get install python3-dev python3-scipy build-essential libcuba3-dev liblapack-dev.

Then just use the makefile in the linux sub-directory.

This should also work on Mint.
  
-----------------------------------------------------------------------------------------------  
MAC OS

  The anaconda version of python causes problems. Python should be installed from brew instead.

-----------------------------------------------------------------------------------------------  
Compute Canada (graham, cedar, Mp2b, beluga)

1) Modules
One needs to add the modules python/3.7.0 and scipy-stack/2018b, or the equivalent for another version of Python3.
If mpi is needed is some scripts, then it should be added as it resides in a distinct module: mpi4py/3.0.0

This is the list of modules installed on my account:

  1) nixpkgs/16.09 (S)   3) python/3.7.0      (t)      5) gcccore/.7.3.0 (H)
  2) StdEnv/2016.4 (S)   4) scipy-stack/2018b (math)   6) gcc/7.3.0      (t)




